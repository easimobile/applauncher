package com.easipos.applauncher

import android.content.Context
import android.view.MotionEvent
import android.view.ScaleGestureDetector
import android.view.View
import kotlin.math.max
import kotlin.math.min

class OnDragTouchListener private constructor(
    private val context: Context,
    view: View,
    parent: View,
    onDragActionListener: OnDragActionListener?
) : View.OnTouchListener {

    companion object {
        private const val ROTATION_DEGREE = 3f
    }

    private var mView: View? = null
    private var mParent: View? = null
    private var isDragging: Boolean = false
    private var isInitialized = false

    private var width: Int = 0
    private var xWhenAttached: Float = 0.toFloat()
    private var maxLeft: Float = 0.toFloat()
    private var maxRight: Float = 0.toFloat()
    private var dX: Float = 0.toFloat()

    private var height: Int = 0
    private var yWhenAttached: Float = 0.toFloat()
    private var maxTop: Float = 0.toFloat()
    private var maxBottom: Float = 0.toFloat()
    private var dY: Float = 0.toFloat()

    private var mOnDragActionListener: OnDragActionListener? = null
    private var mScaleDetector: ScaleGestureDetector? = null
    private var mScaleFactor = 1f
    private var mCurrentX = 0f
    private var mPrevX = 0f
    private var mCurrentY = 0f
    private var mPrevY = 0f
    private var mRotation = 0f

    /**
     * Callback used to indicate when the drag is finished
     */
    interface OnDragActionListener {
        /**
         * Called when drag event is started
         *
         * @param view The view dragged
         */
        fun onDragStart(view: View?)

        /**
         * Called when drag event is completed
         *
         * @param view The view dragged
         */
        fun onDragEnd(view: View?)
    }

    constructor(context: Context, view: View) : this(context, view, view.parent as View, null)

    constructor(context: Context, view: View, parent: View) : this(context, view, parent, null)

    constructor(context: Context, view: View, onDragActionListener: OnDragActionListener) : this(
        context,
        view,
        view.parent as View,
        onDragActionListener
    )

    init {
        initListener(view, parent)
        setOnDragActionListener(onDragActionListener)
    }

    private fun setOnDragActionListener(onDragActionListener: OnDragActionListener?) {
        mOnDragActionListener = onDragActionListener
    }

    private fun initListener(view: View, parent: View) {
        mView = view
        mParent = parent
        isDragging = false
        isInitialized = false
        mScaleDetector = ScaleGestureDetector(context, object : ScaleGestureDetector.OnScaleGestureListener {
            override fun onScaleBegin(detector: ScaleGestureDetector?): Boolean {
                return true
            }

            override fun onScaleEnd(detector: ScaleGestureDetector?) {
            }

            override fun onScale(detector: ScaleGestureDetector?): Boolean {
                mScaleFactor *= detector?.scaleFactor ?: 1f
                mScaleFactor = max(1.0f, min(mScaleFactor, 1.5f))
                view.animate().scaleX(mScaleFactor).scaleY(mScaleFactor).start()
                return true
            }
        })
    }

    private fun updateBounds() {
        updateViewBounds()
        updateParentBounds()
        isInitialized = true
    }

    private fun updateViewBounds() {
        width = mView!!.width
        xWhenAttached = mView!!.x
        dX = 0f

        height = mView!!.height
        yWhenAttached = mView!!.y
        dY = 0f
    }

    private fun updateParentBounds() {
        maxLeft = 0f
        maxRight = maxLeft + mParent!!.width

        maxTop = 0f
        maxBottom = maxTop + mParent!!.height
    }

    override fun onTouch(v: View, event: MotionEvent): Boolean {
        mScaleDetector?.onTouchEvent(event)
        if (isDragging && !mScaleDetector!!.isInProgress) {
            val bounds = FloatArray(4)
            // LEFT
            bounds[0] = event.rawX + dX
            if (bounds[0] < maxLeft) {
                bounds[0] = maxLeft
            }
            // RIGHT
            bounds[2] = bounds[0] + width
            if (bounds[2] > maxRight) {
                bounds[2] = maxRight
                bounds[0] = bounds[2] - width
            }
            // TOP
            bounds[1] = event.rawY + dY
            if (bounds[1] < maxTop) {
                bounds[1] = maxTop
            }
            // BOTTOM
            bounds[3] = bounds[1] + height
            if (bounds[3] > maxBottom) {
                bounds[3] = maxBottom
                bounds[1] = bounds[3] - height
            }

            when (event.action) {
                MotionEvent.ACTION_UP -> onDragFinish()
                MotionEvent.ACTION_MOVE -> {
                    mView?.animate()?.x(bounds[0])?.y(bounds[1])?.setDuration(0)?.start()
//                    mView?.rotation = (Math.toDegrees(atan2((width / 2 - event.x).toDouble(), (height / 2 - event.y).toDouble())).toFloat() - 90f) / 2f

                    mPrevX = mCurrentX
                    mCurrentX = v.x

                    mPrevY = mCurrentY
                    mCurrentY = v.y

                    if (mCurrentX - mPrevX > 0) {
                        mRotation += ROTATION_DEGREE
                    } else if (mCurrentX - mPrevX < 0) {
                        mRotation -= ROTATION_DEGREE
                    } else {
                        if (mCurrentY - mPrevY > 0) {
                            mRotation += ROTATION_DEGREE
                        } else if (mCurrentY - mPrevY < 0) {
                            mRotation -= ROTATION_DEGREE
                        }
                    }

                    if (mRotation > 360f || mRotation < -360f) {
                        mRotation = 0f
                    }
                    mView?.rotation = mRotation
                }
            }
            return true
        } else {
            if (event.action == MotionEvent.ACTION_DOWN) {
                isDragging = true
                if (!isInitialized) {
                    updateBounds()
                }
                dX = v.x - event.rawX
                dY = v.y - event.rawY
                mCurrentX = v.x
                mCurrentY = v.y
                mOnDragActionListener?.onDragStart(mView)
                return true
            }
        }

        return true
    }

    private fun onDragFinish() {
        mOnDragActionListener?.onDragEnd(mView)

        dX = 0f
        dY = 0f
        isDragging = false
    }
}
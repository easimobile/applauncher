package com.easipos.applauncher

data class AppLaunch(val label: String, val intent: String, val drawable: Int)
package com.easipos.applauncher

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity() {

    private var outerApps = ArrayList<AppLaunch>().apply {
        this.add(AppLaunch("", "com.facebook.katana", R.drawable.ic_facebook_box))
        this.add(AppLaunch("", "com.google.android.apps.maps", R.drawable.ic_google_maps))
        this.add(AppLaunch("", "com.waze", R.drawable.ic_waze))
        this.add(AppLaunch("", "com.google.android.youtube", R.drawable.ic_youtube))
        this.add(AppLaunch("", "com.whatsapp", R.drawable.ic_whatsapp))
    }

    private var innerApps = ArrayList<AppLaunch>().apply {
        this.add(AppLaunch("", "com.facebook.katana", R.drawable.ic_facebook_box))
        this.add(AppLaunch("", "com.google.android.apps.maps", R.drawable.ic_google_maps))
        this.add(AppLaunch("", "com.waze", R.drawable.ic_waze))
        this.add(AppLaunch("", "com.google.android.youtube", R.drawable.ic_youtube))
        this.add(AppLaunch("", "com.whatsapp", R.drawable.ic_whatsapp))
    }

    private var dx: Float = 0f
    private var dy: Float = 0f

    private var isMenuExpanded = false
    private var isSubItemMoving = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupViews()
        setupListener()
    }

    private fun setupViews() {
        image_view_menu_play.alpha = 1f

        image_view_menu_category.alpha = 1f
        image_view_menu_category.scaleX = 0f
        image_view_menu_category.scaleY = 0f

        image_view_menu_items.alpha = 1f
        image_view_menu_items.scaleX = 0f
        image_view_menu_items.scaleY = 0f

        image_view_menu.alpha = 0f
    }

    private fun setupListener() {
        image_view_menu_play.setOnClickListener {
            expandMenu()
        }

        // News
        image_view_menu_news_1.setOnClickListener {
            launchIntent(innerApps[0].intent)
        }

        image_view_menu_news_2.setOnClickListener {
            launchIntent(innerApps[1].intent)
        }

        image_view_menu_news_3.setOnClickListener {
            launchIntent(innerApps[2].intent)
        }

        // Video
        image_view_menu_video_1.setOnClickListener {
            launchIntent(innerApps[0].intent)
        }

        image_view_menu_video_2.setOnClickListener {
            launchIntent(innerApps[1].intent)
        }

        image_view_menu_video_3.setOnClickListener {
            launchIntent(innerApps[2].intent)
        }

        // Game
        image_view_menu_game_1.setOnClickListener {
            launchIntent(innerApps[0].intent)
        }

        image_view_menu_game_2.setOnClickListener {
            launchIntent(innerApps[1].intent)
        }

        image_view_menu_game_3.setOnClickListener {
            launchIntent(innerApps[2].intent)
        }

        // Music
        image_view_menu_music_1.setOnClickListener {
            launchIntent(innerApps[0].intent)
        }

        image_view_menu_music_2.setOnClickListener {
            launchIntent(innerApps[1].intent)
        }

        image_view_menu_music_3.setOnClickListener {
            launchIntent(innerApps[2].intent)
        }
    }

    private fun expandMenu() {
        if (!isMenuExpanded) {
            this.isMenuExpanded = true

            image_view_menu_category.animate()
                .scaleX(1f)
                .scaleY(1f)
                .setDuration(400)
//                .setInterpolator(AccelerateInterpolator())
                .start()

            image_view_menu_items.animate()
                .scaleX(1f)
                .scaleY(1f)
                .setDuration(800)
//                .setInterpolator(AccelerateInterpolator())
                .withEndAction {
                    showMenuSubItems()
                    hideExpandedMenu()
                }
                .start()
        }
    }

    private fun showMenuSubItems() {
        image_view_menu.animate().alpha(1f).setDuration(0).withEndAction {
            layout_menu.setOnTouchListener(OnDragTouchListener(this, layout_menu))

            image_view_menu_news_1.visibility = View.VISIBLE
            image_view_menu_news_2.visibility = View.VISIBLE
            image_view_menu_news_3.visibility = View.VISIBLE

            image_view_menu_video_1.visibility = View.VISIBLE
            image_view_menu_video_2.visibility = View.VISIBLE
            image_view_menu_video_3.visibility = View.VISIBLE

            image_view_menu_game_1.visibility = View.VISIBLE
            image_view_menu_game_2.visibility = View.VISIBLE
            image_view_menu_game_3.visibility = View.VISIBLE

            image_view_menu_music_1.visibility = View.VISIBLE
            image_view_menu_music_2.visibility = View.VISIBLE
            image_view_menu_music_3.visibility = View.VISIBLE
        }.start()
    }

    private fun hideExpandedMenu() {
        image_view_menu_play.animate().alpha(0f).setDuration(100).withEndAction {
            image_view_menu_play.visibility = View.GONE
        }.start()

        image_view_menu_category.animate().alpha(0f).withEndAction {
            image_view_menu_category.visibility = View.GONE
        }.setDuration(100).start()

        image_view_menu_items.animate().alpha(0f).setDuration(100).withEndAction {
            image_view_menu_items.visibility = View.GONE
        }.start()
    }

    private fun launchIntent(packageName: String) {
        var intent = packageManager.getLaunchIntentForPackage(packageName)
        if (intent == null) {
            intent = Intent(Intent.ACTION_VIEW).apply {
                this.data = Uri.parse("market://details?id=$packageName")
            }
        }

        try {
            startActivity(intent)
        } catch (ex: Exception) {
            Toast.makeText(this, ex.localizedMessage, Toast.LENGTH_LONG).show()
        }
    }
}
